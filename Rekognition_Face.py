'''
Fecha: 20 de junio de 2019 | Webex
Se utiliza Amazon Rekognition unido a S3
Este programa se encarga de incovar al servicio de Amazon Rekognition
para hacer un analisis de caras: Contar cantidad de personas, reconocer
su estado emocional, si tiene gafas, si es hombre o mujer
y un aproximado de rango de edad
'''
import boto3 #Cliente librería de AWS

#Se le indica a la variable "rekognition"que debe cargar/conectar el sercivio
rekognition = boto3.cliente('rekognition)

def lambda_handler(event, context): #ejecuta la función
    print(event) #Ver el detalle

    #Extraer información del bucket
    bucket = event['Records'][0]['s3']['bucket']['none']
    #Extraer la foto desde la llave key recorriendo diccionarios
    photo = event['Records'][0]['s3']['object']['key']

    #algoritmo de AWS para reconocer faces
    response = rekognition.detect.faces(Image={'S3Object':{'Bucket':bucket, 'Name':photo}},Attributes=['ALL'])

    #Indica la cantidad de personas en la foto
    print('Hay ' + srt(len(response)) + "personas en esta foto.")

    '''Se analiza la foto agregando detalles como:
    Rango de edad, emoción, hombre o mujer
    Se muestra a traves de formato tipo JSON'''
    for faceDetail in response['FaceDetails']:
        print(faceDetails)